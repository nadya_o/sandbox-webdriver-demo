package pages.categories;

import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Button;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Label;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.RadioButton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;
import pages.base.BasePage;

/**
 * Created by pchelintsev on 5/17/2016.
 */
public class Draggable extends BasePage {
    @Override
    protected WebElement elementForLoading() throws Exception {
        return null;
    }

    /**
     * This method drag 'Drag me around' element
     */
    public void dragDragMeAroundElement() throws Exception {

        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='draggable']")),100,20).perform();
        ReportService.ReportAction("Element was dragged",true);
    }

    /**
     * This method drag element only verticaly
     */
    public void dragVerticalyElement() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//div[@id='draggabl']")),0,150).perform();
        ReportService.ReportAction("Element was dragged vertically",true);
    }

    /**
     * This method drag element only horizontally
     */
    public void dragHorizontallyElement() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//div[@id='draggabl2']")),300,0).perform();
        ReportService.ReportAction("Element was drugged horziontally",true);
    }

    /**
     * This method drag element with cursor in a center
     */
    public void dragMouseInCentr() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='drag']/p")),100,20).perform();
        ReportService.ReportAction("Element was dragged to center",true);
    }

    /**
     * This method drag element with cursor in a left corner
     */
    public void dragMouseInLeftCorner() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='drag2']/p")),150,40).perform();
        ReportService.ReportAction("Element was dragged to left corner",true);
    }

    /**
     * This method drag element with cursor in a bottom
     */
    public void dragMouseInBottom() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='drag3']/p")),200,60).perform();
        ReportService.ReportAction("Element was dragged to bottom",true);
    }

    /**
     * This method drag element in in different directions
     * @throws InterruptedException
     */
    public void dragTriger() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='dragevent']")),100,100).perform();
        Thread.sleep(1000);
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='dragevent']")),50,50).perform();
        Thread.sleep(1000);
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='dragevent']")),-50,-50).perform();
        Thread.sleep(1000);
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='dragevent']")),-150,-150).perform();
        Thread.sleep(1000);
        ReportService.ReportAction("Element was dragged",true);

    }

    /**
     * This method drag elements and sorted them
     * @throws InterruptedException
     */
    public void dragAndSort() throws Exception {
        WebElement element = driver.findElement(By.xpath(".//ul[@id='sortablebox']/li[contains(text(),'Item 1')]"));
        WebElement to = driver.findElement(By.xpath(".//ul[@id='sortablebox']/li[contains(text(),'Item 3')]"));
        (new Actions(driver)).dragAndDrop(element,to).perform();
        Thread.sleep(1000);

         element = driver.findElement(By.xpath(".//ul[@id='sortablebox']/li[contains(text(),'Item 4')]"));
         to = driver.findElement(By.xpath(".//ul[@id='sortablebox']/li[contains(text(),'Item 2')]"));
        (new Actions(driver)).dragAndDrop(element,to).perform();
        Thread.sleep(1000);

         element = driver.findElement(By.xpath(".//ul[@id='sortablebox']/li[contains(text(),'Item 5')]"));
         to = driver.findElement(By.xpath(".//ul[@id='sortablebox']/li[contains(text(),'Item 3')]"));
        (new Actions(driver)).dragAndDrop(element,to).perform();
        Thread.sleep(1000);

         element = driver.findElement(By.xpath(".//ul[@id='sortablebox']/li[contains(text(),'Item 2')]"));
         to = driver.findElement(By.xpath(".//ul[@id='sortablebox']/li[contains(text(),'Item 1')]"));
        (new Actions(driver)).dragAndDrop(element,to).perform();
        Thread.sleep(1000);
        ReportService.ReportAction("Element was dragged",true);

    }

}
