package pages.categories;

import arp.ReportService;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Button;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import pages.base.BasePage;

/**
 * Created by ivnytska on 17-May-16.
 */
public class Resizable extends BasePage {
    @Override
    protected WebElement elementForLoading() throws Exception {
        return null;
    }

    /**
     *This method increase the size of resizable element in 'Default functionality' section
     */

    public void increaseElementDefault() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizable']/div[3]")), 500, 158).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method decrease the size of resizable element in 'Default functionality' section
     */

    public void decreaseElementDefault() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizable']/div[3]")), -450, -160).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method increase resizable element to middle size in 'Default functionality' section
     */

    public void increaseMiddleElementDefault() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizable']/div[3]")), 200, 200).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method increase the size of resizable element in 'Animate' section
     */

    public void increaseElementAnimate() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizableani']/div[3]")), 380, 100).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method decrease the size of resizable element in 'Animate' section
     */

    public void decreaseElementAnimate() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizableani']/div[3]")), -250, -50).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method increase resizable element to middle size in 'Animate' section
     */

    public void increaseMiddleElementAnimate() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizableani']/div[3]")), 320, 5).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method increase the size of resizable element in 'Constrain resize area' section
     */

    public void increaseElementConstrain() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizableconstrain']/div[3]")), 10, 150).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method decrease the size of resizable element in 'Constrain resize area' section
     */

    public void decreaseElementConstrain() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizableconstrain']/div[3]")), 0, -75).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method increase the size of resizable element in 'Helper' section
     */

    public void increaseElementHelper() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizable_helper']/div[3]")), 400, 158).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method decrease the size of resizable element in 'Helper' section
     */

    public void decreaseElementHelper() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizable_helper']/div[3]")), -450, -160).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method increase resizable element to middle size in 'Helper' section
     */

    public void increaseMiddleElementHelper() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizable_helper']/div[3]")), 200, 180).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method increase the size of resizable element in 'Max/Min size' section
     */

    public void increaseElementMaxMinSize() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizable_max_min']/div[3]")), 400, 158).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }

    /**
     *This method decrease the size of resizable element in 'Max/Min size' section
     */

    public void decreaseElementMaxMinSize() throws Exception {
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='resizable_max_min']/div[3]")), -450, -160).perform();
        ReportService.ReportAction("Elvement was resized", true);
    }
}