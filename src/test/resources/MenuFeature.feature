@ST_8800 @Feature_questions
Feature: Menu

@SC_37069 @High @Question @Menu
Scenario: Menu
Given I'm on main page
And I select "Menu" category
And I click on "Simple Menu" button
And I click on each menu section one by one
And I click on "Menu With Sub Menu" button
And I click on each menu section and sub-section one by one