@ST_8794 @Feature_Accordion
Feature: accordion test

@SC_37061 @High @Accordion @DefaultFunctionality
Scenario: Default functionality section 1 select
Given I'm on main page
And I select "Accordion" category
When I select "Section 1" section
And I select "Section 2" section
And I select "Section 3" section
And I select "Section 4" section
Then I check that current section is "Section 4"

@SC_37062 @High @Accordion @CustomizeIcons
Scenario: Customize icons section 2 select
Given I'm on main page
And I select "Accordion" category
And I select "Customize icons" tab
When I select "Section 2" section
And I check that icons are visible
And I click on 'Toggle icons' button
And I check that icons are not visible
And I click on 'Toggle icons' button
And I check that icons are visible

@SC_37063 @High @Accordion @FillSpace
Scenario: Fill Space section 1 select
Given I'm on main page
And I select "Accordion" category
And I select "Fill Space" tab
When I select "Section 1" section
And I select "Section 2" section
And I select "Section 3" section
And I select "Section 4" section
Then I check that current section is "Section 4"