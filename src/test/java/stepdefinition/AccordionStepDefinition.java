package stepdefinition;

import cucumber.api.java.en.*;
import arp.CucumberArpReport;
import arp.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.categories.Accordion;

import java.util.Date;

/**
 * Created by kozlov on 5/18/2016.
 */
public class AccordionStepDefinition extends PageInstance {

    @Autowired
    private
    Accordion accordion;

    @When("^I select \"([^\"]*)\" tab$")
    public void iSelectTab(String tabName) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            accordion.selectTab(tabName);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            Date x = new Date();
            ReportService.ReportAction(e.getMessage(), false);
            System.out.println(new Date().getTime()-x.getTime());
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I select \"([^\"]*)\" section$")
    public void iSelectSection(String sectionName) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            accordion.selectSection(sectionName);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @Then("^I check that current section is \"([^\"]*)\"$")
    public void iCheckSection(String sectionName) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            accordion.checkCurrentSection(sectionName);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I check that icons are visible")
    public void iCheckSectionIconsVisible() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            accordion.checkIconsAreVisible();
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I check that icons are not visible")
    public void iCheckSectionIconsNotVisible() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            accordion.checkIconsAreNotVisible();
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click on 'Toggle icons' button")
    public void iToggleButtons() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            accordion.toggleIcons.click();
            ReportService.ReportAction("Button was clicked",true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
